(set-logic UF)

;; Signature
(declare-sort t 0)
(declare-fun one () t)
(declare-fun star (t t) t)

;; Axioms
;; associativeAxiom
(assert (forall ((x0 t) (x1 t) (x2 t))
   (= (star x0 (star x1 x2)) (star (star x0 x1) x2))))
;; leftNeutralAxiom
(assert (forall ((x0 t)) (= (star one x0) x0)))
;; rightNeutralAxiom
(assert (forall ((x0 t)) (= (star x0 one) x0)))

(echo "expect sat")
(check-sat)
