package TestPackage.Boolean;

/**
* Simple Binop
*/
concept Binop = {
    type T;

    function binop(l: T, r: T) : T;
};

/**
* Extend Binop to make monoid
*/
concept Monoid = {
    use Binop [T => M];
    //> identity element for the monoid
    function neutral() : M;


    /**
    * Binop neutral under identity
    */
    axiom /* anot */ identity(a:M) {
        
        assert binop(a, neutral()) == a;
    };

    /**
    * Binop is associative
    */
    axiom associative( a: M, b:M, c:M){
        assert binop(binop(a, b), c) == binop(a, binop(b, c));
    };
};


concept Boolean {
    use Monoid [M => B, binop => or, neutral => False];
    
    function true() : B;
    
    function not(p : B) : B;
};