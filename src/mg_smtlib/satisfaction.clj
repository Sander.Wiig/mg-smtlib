(ns mg-smtlib.satisfaction
  (:refer-clojure :exclude [type, use, assert])
  (:require [mg-smtlib.spec :refer [defspec
                                    type
                                    function
                                    axiom
                                    assert]]
            [mg-smtlib.util.error :as e]))