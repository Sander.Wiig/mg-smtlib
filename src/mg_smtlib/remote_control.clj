(ns mg-smtlib.remote-control 
  (:refer-clojure :exclude [sort, use, assert])
  (:use [mg-smtlib.spec])
  (:require [mg-smtlib.stf :as stf]
            [clojure.core.match :refer [match]]
            [clojure.pprint :refer [pprint]]
            [mg-smtlib.util.error :as e]))

#_{:clj-kondo/ignore [:unresolved-symbol]}


(defn perm
  [ls]
  (filter (fn [[x y]] (not= x y)) (for [x ls
                                        y ls]
                                    [x y])))
(defn free
  [s [& op-names] spec]
  ;;Signature side
  (->> spec
       ;;Signature
       (stf/add-sort s)
       (#(reduce
          (fn [spec' op]
            (stf/add-op `(~'op ~op [] ~s) spec'))
          %
          op-names))

       ;;Add distinctiveness Axioms 
       (stf/add-decl
        `(~'axiom ~'distinctiveness []
                  (~'assert
                   (~'not
                    (~'or
                     ~@(map
                        (fn [[op1 op2]]
                          `(~'= ~op1 ~op2))
                        (perm op-names)))))))))
        (T (Foo "1" "2")) 
(first '(foo 1 2))
(defn add-param
  [s [_ n param ret]]
  `(~'op ~n ~(conj param `(~(keyword (gensym (name s))) ~s)) ~ret)
  )

(defn op->name
  [[_ n _ _ ]]
  n)
(defn augment
  [s ops spec]
  (->> spec
       ;;Add sort if it doesnt exist
       (stf/add-sort s)
       ;; Augment a operation
       (#(let [[_ name & body] %
               body' (mapv (fn [decl]
                            (if (and (op? decl)
                                     (some #{(op->name decl)} ops))
                              (add-param s decl)
                              decl)) body)]
           `(~'defspec ~name
                       ~@body')))))


#_{:clj-kondo/ignore [:unresolved-symbol]}
(comment
  (def spec1
    '(defspec remote-control
       ;; Sorts
       (sort Button)
       (sort Signal)
       
       ;; Operations
       (op companyId [] Signal)
       (op deviceType [] Signal)
       (op codeOf [:button Button] Signal)
       (op buttonCode [:button Button] Signal)
       (op _++_ [:s1 Signal :s2 Signal] Signal)
  
       (axiom ax1 [:b Button]
              (assert (= (codeOf b)
                         (_++_ (_++_ companyId
                                     deviceType)
                               (buttonCode b)))))))
  (identity spec1)

  (augment 'Mode ['deviceType] spec1)
  (free
   'Button
   ['b0,'b1,'b2,
    'b3,'b4,'b5,
    'b6,'b7,'b8,
    'b9,'b-on-off,
    'up,'dn] spec1)
  (free
   'Mode
   ['TV, 'DVD, 'SAT]
   spec1)
  
  (->> spec1
        (augment 'Mode ['deviceType])

        (free
         'Mode
         ['TV, 'DVD, 'SAT])
       
        (free
         'Button
         ['b0,'b1,'b2,
          'b3,'b4,'b5,
          'b6,'b7,'b8,
          'b9,'b-on-off,
          'up,'dn]))
  
   ((comp
     (partial free
              'Button
              ['b0,'b1,'b2,
               'b3,'b4,'b5,
               'b6,'b7,'b8,
               'b9,'b-on-off,
               'up,'dn])

     (partial free
              'Mode
              ['TV, 'DVD, 'SAT])
     (partial augment 'Mode ['deviceType])) 
    spec1)
  )