(ns mg-smtlib.stf
  (:refer-clojure :exclude [sort, use, assert])
  (:require [mg-smtlib.spec :as spec]
            [clojure.walk :refer [walk, postwalk, prewalk]]
            [clojure.core.match :refer [match]]
            [clojure.pprint :refer [pprint]]
            [mg-smtlib.util.error :as e]))




;;STF stuff


#_(defmacro defSTF
    [&name body]
    (let  [&sorts (spec->sorts &spec)])
    `(fn
       [&spec]
       ~@body))



;;TODO: Validity checks
(defn add-decl
  [decl spec]
  (let [[_ name & body] spec]
    (if-not (some #{decl} (spec/spec->decl spec) )
      `(~'defspec ~name
                  ~decl
                  ~@body)
      spec)))

(defn add-sort
  [s spec]
   (add-decl `(~'sort ~s) spec)
  )

(defn add-op
  ([op args ret spec]
   (add-decl `(~'op ~op ~args ~ret) spec))
  ([op spec]
   (add-decl op spec)))


(defn rename
  [old-symbol new-symbol spec]
  (postwalk #(if (and (symbol? %) (= % old-symbol))
               new-symbol
               %) spec))

(defn use
  [spec use1]
  (let [use2(spec/spec->decl use1)]
    (reduce #(add-decl %2 %1)
            spec
            use2)))

;; STF's from STF paper
;;? Macros ?


(defn generated
  [spec])

(defn lift
  [spec])

(defn union
  [spec])

#_(defn rename
   [phi spec])

(defn error
  [spec])

(defn augment'
  [spec])

(defn augment
  [spec])




;; Magnolia STFs
#_(defn use
  [spec])

(defn plainify
  [spec])

(defn goodness
  [spec])

#_{:clj-kondo/ignore [:unresolved-symbol]}
(comment
  (some #{'(op binop [:r T :l T] T)} ['(sort T) '(op binop [:r T :l T] T) '(axiom test [:a T :b T] (assert (= (binop :a :b) (binop :b :a))))])

  (decompose '(defspec foo
                (sort T)
                (sort U)
                (op a [] T)))

  (def binop '(defspec Binop
                (sort T)
                (op binop [:r T :l T] T)
                (axiom test [:a T :b T]
                       (assert (= (binop :a :b) (binop :b :a))))))


  (def monoid '(defspec Monoid
                 (op Neutral [] T)

                 (axiom identity
                        [(:a T)]
                        (assert (== :a (binop a neutral))))
                 
                 (axiom associative
                        [(:a T), (:b T), (:c T)]
                        (assert (== (binop (binop :a :b) :c)
                                    (binop :a (binop :b :c)))))))


  (add-sort 'T binop)
  (add-op '(op binop [:r T :l T] T) binop)
  (rename 'Neutral 'Id (rename 'binop 'add (use monoid binop)))
  (rename 'M 'T monoid)
  (add-op '(op binop [:r T :l T] T) (add-sort 'T monoid))
  (do
    (pprint (spec/decompose binop))
    (pprint '---)
    (pprint (spec/decompose monoid)))



    ;;S1 use S2
  
  )