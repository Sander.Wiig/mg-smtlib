(ns mg-smtlib.spec
  (:refer-clojure :exclude [sort, assert])
  (:require [clojure.string :as string]
            [mg-smtlib.util.error :as e]
            [clojure.core.match :refer [match]]
            [clojure.walk :refer [walk, postwalk, prewalk]]
            [clojure.pprint :refer [pprint]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;    AST DOCUMENTATION    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; AST Nodes
;; ----------------------------
;; SPEC: 
;; ```
;; {:node :spec
;;  :name <spec-name>
;;  :body [<spec-decls>]}
;; ```
;;
;; DECL
;; - SORT/TYPE: 
;; ```
;; {:node :defsort
;;  :name <sort-name>}
;; ```
;;
;; - FUNCTION: 
;; ```
;; {:node :defn
;;  :name <function-name>
;;  :param [{:node :param
;;           :name <param-name> 
;;           :sort <type-name>}]
;;  :return <type-name>}
;; ```
;;
;; - AXIOM: 
;; ```
;; {:node :axiom
;;  :name <axiom-name>
;;  :param [{:node :param
;;           :name <param-name> 
;;           :sort <type-name>}]
;;  :body [<term>]}
;; ```
;;
;; TERMS
;;
;; - ASSERT
;; ```
;; {:node :assert
;;  :expr <expr>}
;; ```
;;
;; - LET
;; ```
;; {:node :let
;;  :bindings <(:<var-name> <expr>)+>
;;  :term <term>}
;; ```
;;
;; - IF
;; ```
;; {:node :if
;;  :test <expr>
;;  :then <term>
;;  :else <term>}
;; ```
;;
;; - WHEN
;; ```
;; {:node :when
;;  :test <expr>
;;  :do <term>}
;; ```
;;
;; - DO BLOCK
;; ```
;; {:node :do
;;  :body [<term>]}
;; ```

;;
;; EXPR
;; - VAR
;; ```
;; {:node :var
;;  :name <var-name>}
;; ```
;;
;; - FUNCTION CALL
;; ```
;; {:node :fn-call
;;  :name <function-name>
;;  :args [<expr>]}
;; ```


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;    SMT-LIB GENERATION    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn expr->smt
  [e]
  (cond
    (= :fn-call (:node e)) `(~(symbol (:name e)) ~@(mapv expr->smt (:args e)))
    (= :var (:node e)) `~(symbol (:name e))
    :else nil))

(defn term->smt
  [term]
  (cond
    (= :assert (:node term)) (expr->smt (:expr term))
    (= :when (:node term)) `(~'=> ~(expr->smt (:test term)) ~(expr->smt (:do term)))
    #_(= :assert (:node term))
    :else nil))


(defn param->smt
  [p]
  `(~(symbol (name (:name p))) ~(symbol (:sort p))))
  ;; {:node :axiom
  ;;  :name <axiom-name>
  ;;  :params [{:node :param
  ;;           :name <param-name> 
  ;;           :sort <sort-name>}]
  ;;  :body [<term>]}
(defn axiom->smt
  [ax]
  `(~'assert (~'forall ~(map param->smt (:params ax)))
             ~@(mapv term->smt (:body ax))))

(defn body->smt
  [decl]
  (cond
    (= :defsort (:node decl)) `(~'declare-sort ~(symbol (:name decl)) 0)
    (= :defn    (:node decl)) `(~'declare-fun  ~(symbol (:name decl)) ~(mapv #(symbol (:sort %)) (:param decl)) ~(symbol (:return decl)))
    (= :axiom   (:node decl))  (axiom->smt decl)
    :else nil))

(defn spec->smt
  [s]
  ;; {:node :spec
  ;;  :name <spec-name>
  ;;  :body [<spec-decls>]*}
  (when (= (:node s) :spec)
    (vec (conj (map body->smt (:body s)) '(set-logic UF)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;    TERMS AND EXPRESSIONS    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn expr
  [&expr]
  (match [&expr]
    ;; Function application
    [([(f :guard #(symbol? %)) & _] :seq)] (let [args (mapv expr (rest &expr))]
                                             (if (some :error args)
                                               (some :error args)
                                               `{:result {:node :fn-call
                                                          :name ~(name f)
                                                          :args ~(mapv :result args)}}))
    ;;Sometimes constant functions are called outside a seq so need a case for that
    [(f :guard #(symbol? %))] `{:result {:node :fn-call
                                         :name ~(name f)}}
    ;; Variable decl
    [(v :guard #(keyword? %))] `{:result {:node :var
                                          :name ~v}}
    :else `{:error ~(str "Error: `" &expr "` is not recognized as a valid expression.")}))


(defmacro assert
  [&expr]
  (if-let [err (some :error (map expr &expr))]
    err
    `{:result {:node :assert
               :expr ~(:result (expr &expr))}}))


;;TODO: error handling
(defmacro if'
  [&test &then &else]
  `{:result {:node :if
             :test ~(:result (expr &test))
             :then ~(:result (expr &then))
             :else ~(:result (expr &else))}})

;;TODO: error handling
(defmacro when'
  [&test &do]
  `{:result {:node :when
             :test ~(:result (expr &test))
             :do ~(:result (expr &do))}})
;;TODO: error handling
(defmacro let'
  [&bindings & stmts]
  `{:result {:node :let
             :bindings ~(->> &bindings
                             (partition 2) ; partition into arg-type tuples
                             (reduce ;build arg map
                              #(assoc %1
                                      (first %2)
                                      (:result (expr (last %2))))
                              {}))
             :term ~(mapv #(->> % macroexpand :result) stmts)}})

;;TODO: error handling
;;Don't like this very much, want to remove it tbh
(defmacro do'
  [& stmt]
  `{:result {:node :do
             :body ~(mapv #(->> % macroexpand :result) stmt)}})

;;? Don't like this
(defmacro call
  [&name & args]
  (if (symbol? &name)
    (let [args (mapv #(if (keyword? %); Args are either function calls or vars(clj keywords)
                        ;;if keyword return a var call
                        `{:result {:node :var :name ~(name %)}}
                        ;;else macroexpand the arg
                        (macroexpand %))
                     args)]
      ;; Check for errors in args
      (if (some :error args)
        (some :error args);return error
        `{:result {:node :fn-call
                   :name ~(name &name)
                   :args ~(mapv :result args)}}))
    {:error (str "Error: " &name " is not a symbol")}))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;    SPECIFICATION MACROS    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;TODO: this should expand into a def so it is set in the enviroment
(defmacro defspec
  [&name & decls]
   ;; If no errors then return AST node
  (let [decl' (mapv macroexpand decls)
        decl-error (some :error decl')]

    (if-not decl-error
      `{:result {:node :spec
                :name ~(str &name)
                :body ~(mapv :result decl')}}
      {:error decl-error})))

(defn param
  [&params]
  (if (= (mod (count (flatten &params)) 2) 0)
    {:result (->> &params
                  flatten ; Flatten arg-list for syntax flexibility
                  (partition 2) ; partition into arg-type tuples
                  (reduce ;build arg map
                   #(conj %1 (assoc {}
                                    :node :param
                                    :name (first %2)
                                    :sort (str (last %2))))
                   []))}
    {:error "Arg list is not divisible by two"}))


(defmacro sort
  "A sort declaration in a spec"
  [&name]
  (if (symbol? &name)
    `{:result {:node :defsort
               :name ~(str &name)}}
    `{:error (str &name " is not a valid typename")}))

(defmacro op
   ;;Unguarded 
  ([&name &params &ret]
   (if-let [err (:error (param &params))]
     #break
      {:error err}
     `{:result {:node :defn
                :name ~(str &name)
                :param ~(:result (param &params))
                :return ~(str &ret)}}))
  ;;Guarded
  ([&name &params &ret &guard &guard-expression]

   (if (= &guard :guard)
     #break
      (let [res (macroexpand `(op ~&name ~&params ~&ret))
            gex (expr &guard-expression)]
        (if (and (= &guard :guard)
                 (:result res)
                 (:result gex))
          (assoc res :guard (:result gex))
          (or
           (:error res)
           (:error gex))))
     {:error (str "Error: Malformed guard in `" &form "`.")})))


;;TODO: add guard!!!!
;;? maybe make a guard macro
;;? - Would make it more general
;;? - Makes syntax more complicated

(defmacro axiom
  [&name &ax-args & &body]
  (let [&ax-args' (param &ax-args)
        &body' (mapv macroexpand &body)]
    (if-let [err (or
                  (:error &ax-args')
                  (some :error &body'))]
      {:error err}
      `{:result {:node :axiom
                 :name ~(str &name)
                 :params ~(:result &ax-args')
                 :body ~(mapv :result &body')}})))

(defn sort?
  [decl]
  (match [decl]
    [(['sort (_ :guard #(symbol? %))] :seq)] decl
    :else nil))

(defn op?
  [decl]
  (match [decl]
    [(['op (&name :guard #(symbol? %)) [& &params] (&ret :guard #(symbol? %))] :seq)] decl
    [(['op (&name :guard #(symbol? %)) [& &param] (&ret :guard #(symbol? %)) (&guard :guard #(keyword? %)) &guard-expression] :seq)] decl
    :else nil))

(defn axiom?
  [decl]
  (match [decl]
    [(['axiom (name :guard #(symbol? %)) [& params] &expr] :seq)] decl
    :else nil))


(defn spec->decl
  [[_ &name & decls]]
  (vec decls))

(defn spec->name
  [[_ &name & _]]
  &name)
(defn spec->sorts
  "Returns a vector of all the sort declarations in a spec"
  [[_ _ & decls]]
  (filterv #(sort? %) decls))

(defn spec->ops
  [[_ _ & decls]]
  (filterv #(op? %) decls))

(defn spec->axioms
  [[_ _ & decls]]
  (filterv #(axiom? %) decls))

(defn decompose
  [spec]
  (assoc {}
         :spec (name (spec->name spec))
         :sorts (spec->sorts spec)
         :ops (spec->ops spec)
         :axioms (spec->axioms spec)))


#_{:clj-kondo/ignore [:unresolved-symbol, :type-mismatch]}
(comment


  (clojure.walk/prewalk-demo  '(let' [:a foo] (assert (= foo :a))))

  (if' (= a b)
       (= foo :a)
       (= foo :a)) 

  (when' (= a b)
         (= foo :a))

  (op foo [:l T :r T] T :guard (foo :l :r))
  (guard (function op))

  (param '[:l T :r T])
  (expr '(< :l :r))

  (def binop (defspec Binop
               (sort T)
               (op binop [:r T :l T] T)
               (axiom test [:a T :b T]
                      (assert (= (binop :a :b) (binop :b :a))))))


  (def monoid (defspec Monoid
                (op Neutral [] T)
                (axiom identity
                       [(:a M)]
                       (assert
                        (== :a (binop a neutral))))

                (axiom associative
                       [(:a M), (:b M), (:c M)]
                       (assert (== (binop (binop :a :b) :c)
                                   (binop :a (binop :b :c)))))))
  
  (e/>>=
   binop
   (partial spec->smt))

  (spec->smt (:result monoid))




  (defn seq-param
    [a]
    (if (= (mod (count a) 2) 0)
      {:result (->> a
                    (partition 2)
                    (reduce
                     #(assoc %1
                             (keyword (first %2))
                             (str (last %2)))
                     {}))}
      {:error "Arg list is not divisible by two"}))


  (defn tuple-param
    [a]
    (if (every? #(and seq? (= 2 (count %))) a)
      {:result (reduce
                #(assoc %1
                        (keyword (first %2))
                        (str (last %2)))
                {}
                a)}
      {:error (str "`" a "` is not a valid arg list")}))


  (defn map-param
    [&params]
    (if (= (mod (count (flatten &params)) 2) 0)
      {:result (->> &params
                    flatten ; Flatten arg-list for syntax flexibility
                    (partition 2) ; partition into arg-type tuples
                    (reduce ;build arg map
                     #(conj %1 (assoc {}
                                      :node :param
                                      :name (first %2)
                                      :sort (str (last %2))))
                     []))}
      {:error "Arg list is not divisible by two"}))


  (map-param '[:a M, :b M, :c M])
  (tuple-param '[(a M), (b M), (c M)])





  ;;Statements are S-expression
  #_(defn stmt
      [&stmt]

      (e/return
       (if (seq? &stmt)
         (match [&stmt]
           [()]

           :else {:error (str "Error: `" &stmt "` is not recognized as valid term.")})
         {:error (str "Error: `" &stmt "` is not a seq.")})))
  )