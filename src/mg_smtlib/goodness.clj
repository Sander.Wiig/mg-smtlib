#_{:clj-kondo/ignore [:unresolved-symbol, :invalid-arity]}
 (comment

 (axiom rem-axiom [:x0 Number :x1 Number]
        (assert (when' (distinct :x1 zero)
                       (when' (le zero :x0)
                              (le zero (rem :x0 :x1))))))
   
   (assert (not (forall ((x0 number) (x1 number))
                        (=> (distinct x1 zero)
                            (=> (le zero x0)
                                (le zero (rem x0 x1)))))))
   
   (=> (distinct x1 zero)
       (=> (le zero x0)
           (le zero (rem x0 x1))))

   (when (distinct x1 zero)
     (when (le zero x0)
       (le zero (rem x0 x1))))




   (=> (distinct x1 zero)
       (=> (and
            (goodNumber x0)
            (goodNumber x1))
           (=> (goodNumber (rem x0 x1))
               (=> (le zero x0)
                   (le zero (rem x0 x1))))))

   (when (distinct x1 zero)
     (when (and (goodNumber x0) (goodNumber x1))
       (when (goodNumber (rem x0 x1))
         (when (le zero x0)
           (le zero (rem x0 x1))))))


;; Algo v.1
;; 1. Traverse form.
;; 2. When at implication, get funcs/vars used in branches.
;; 3. If Func/var used in both branches, then create goodness check on those funcs/vars
;; 4. Go to next implication.

;; Algo v.2
;; 1. Traverse form.
;; 2. When at implication, get funcs used in branches.
;; 3. If Func used in both branches, then create goodness check on those funcs
;; 4. Go to next implication.
;; 5. When done create goodness check at start for all variables in axiom.

  ;; Algo generated Axiom
   (when (and
          (goodNumber x0)
          (goodNumber x1))
     (when (goodNumber zero)
       (when (distinct x1 zero)
         (when (goodNumber (rem x0 x1))
           (when (le zero x0)
             (le zero (rem x0 x1)))))))

  ;;SMT Version
   (=> (and
        (goodNumber x0)
        (goodNumber x1))
       (=> (goodNumber zero)
           (=> (distinct x1 zero)
               (=> (goodNumber (rem x0 x1))
                   (=> (le zero x0)
                       (le zero (rem x0 x1)))))))
   )