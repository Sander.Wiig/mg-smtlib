(ns mg-smtlib.util.error
  "Contains a simple implemenmtation of an error"
  (:require [clojure.core.match :refer [match]]
            [clojure.string :as string]))



;;;;;;;;;;;;;;;;;;;;;;;
;;    Error Monad    ;;
;;;;;;;;;;;;;;;;;;;;;;;

;; Note [The Error Monad]
;; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;; TODO: 
;; An error monad is one of two possible values. 
;; It is either `:error` or `:result`, where `:error` is supposed to be a description of the error
;; and `:result` is the result of some computation.
;;
;; `error` should support the following operations,which adhear to certain properties.
;; 
;; `error` is a Functor
;; -------------------
;; A functor is a structure preserving morphism(generalized term for function)
;; A type f is a Functor if it provides a function `fmap` which,
;; given any types `a` and `b` lets you apply any function from `(a -> b)` to turn an `f a` into an `f b`,
;; preserving the structure of f. 
;;
;; `fmap :: (a->b) -> m a -> m b
;; `fmap` has the following properties
;;
;; - Identity: `(fmap identity a) == (identity a)`
;;
;; - Composition:(fmap (comp f g) a) == (->> a (fmap f) (fmap g))
;;
;; `error` is a applicative functor/lax monoidal functor
;; -------------------
;; TODO: Implement the applicative stuff
;;
;; - Identity: `(<*> (pure id a) v) == v`
;;
;; - Composition `pure (.) <*> u <*> v <*> w = u <*> (v <*> w)`
;;
;; - Homomorphism`pure f <*> pure x = pure (f x)`
;;
;; - Interchange: `u <*> pure y = pure ($ y) <*> u`
;;
;;
;;
;; Instances of 'Monad' should satisfy the following:
;; 
;; From haskell 
;; [Left identity]  `(bind (return a) k)  =  (k a)`
;; [Right identity] `(bind m (partial return)) =  m`
;; [Associativity]  `m '>>=' (\\x -> k x '>>=' h)  =  (m '>>=' k) '>>=' h`

;;;;;;;;;;;;;;;;;;;
;;    ERROR    ;;
;;;;;;;;;;;;;;;;;;;

(defn result
  [r]
  {:result r})

(defn error
  [e]
  {:error e})


(defn e->error?
  [m]
  (:error m))

(defn e->result?
  [m]
  (:result m))

(defn error?
  [m]
  (or
   (e->error? m)
   (e->result? m)))


;;;;;;;;;;;;;;;;;;;
;;    FUNCTOR    ;;
;;;;;;;;;;;;;;;;;;;

(defn fmap
  "Applies a function f to a the error monad."
  [f e]
  (if-let [x (e->result? e)]
    (result (f x))
    e))

;;;;;;;;;;;;;;;;;;;;;;;
;;    APPLICATIVE    ;;
;;;;;;;;;;;;;;;;;;;;;;;

(defn pure
  [a]
  (result a))

(defn liftA2
  [f a b]
  (if-let [a' (e->result? a)]
    (if-let [b' (e->result? b)]
      (pure (f a' b'))
      b)
    a))

(defn <*>
  "Sequential application."
  ;;TODO:
  [f a]
  (liftA2 identity f a))

;;;;;;;;;;;;;;;;;
;;    MONAD    ;;
;;;;;;;;;;;;;;;;;

;; m (m a) -> m a
;; :error (m a) -> (:error (m a))
(defn join
  "join m = m >>= id"
  [m]
  ;;Strip outer layer
  (let [m' (e->result? m)]
    (if (and m' (error? m')) ;;check that inner is a monad
      m
      m)))


;; a -> m a
(defn return
  "Lifts a value to an error monad"
  [a]
  (if-not (error? a)
    {:result a}
    a))

(defn >>=
  "Error monad bind"
  [m f]
  (if-let [e (e->error? m)]
    ;;return error if its an error
    e
    ;; Apply the function to result of m
    (return (f (e->result? m)))))


;;TODO:
(defn liftM
  [f m]
  (>>= m f))




#_(defmacro ifM
    "version of if that treats :error as false"
    ([test then]
     (if (e->result? test)
       then
       nil))
    ([test then else?]
     (if (e->result? test)
       then
       else?)))

#_(defmacro ifM-let
    "bindings => binding-form test

  If test is true, evaluates then with binding-form bound to the value of 
  test, if not, yields else"
    ([bindings then]
     `(ifM-let ~bindings ~then nil))
    ([bindings then else & oldform]
     (let [form (bindings 0) tst (bindings 1)]
       `(let [temp# ~tst]
          (ifM temp#
               (let [~form temp#]
                 ~then)
               ~else)))))
